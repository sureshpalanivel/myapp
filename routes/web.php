<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Collection;
use Illuminate\Support\Str;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Builder;


use App\User;
use App\Address;
use App\Post;
use App\Tag;
use App\Mechanic;
use App\Car;
use App\Owner;
use App\Video;
use App\Comment;
use App\Demo;

use Illuminate\Support\Facades\Cookie;

// #Every time Run after you call where you want and return every time new result
// app()->bind('random',function(){
//     return Str::random();
// });

// print('I AM Bind');
// echo "<br>";
// dump(app()->make('random'));
// dump(app()->make('random'));

// #Only Once Run after you call where you want and return same result
// app()->singleton('rand',function(){
//     return Str::random();
// });

// print('I AM Singleton');
// echo "<br>";
// dump(app()->make('rand'));
// dd(app()->make('rand'));

//dd(app()->make('Hello'));

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/', ['as'=> 'firstr', 'uses'=>function () {     
//     return 'Good Luck';
// }]);

Route::get('/first', ['as'=> 'firstr', 'uses'=>function (Request $request) {
    // return 'Good Luck';
    
    // print_r(request()->all());
    // var_dump(request()->all());
    // dd(request()->all());
    return [$request->path(),
           $request->getHttpHost(),
           $request->method(),
           $request->has('first'),
           $request->url(),
           $request->is('first'),
           $request->getSchemeAndHttpHost(),
           $request->getHttpHost(),
           app_path(),
           base_path(),
           config_path(),
           database_path(),
           public_path(),
           resource_path(),
           storage_path(),
          ];
   
    #return 'Good Luck';
}]);

Route::get('/qqq', function (Request $request) {
    print_r($request->all());
    print('<br>');
    print($request->url());
    print('<br>');
    print($request->fullUrl());
    print('<br>');
    print((boolean) $request->isMethod('post'));
    print('<br>');
    dd((boolean) $request->method);
    print('<br>');
    dd ((boolean)$request->is('/'));
    if ($request->is('/')) {
        return 'WWW';
    }else{
        return 'Haii';
    }
});

Route::get('/test', ['as'=> 'testr', 'uses'=>function (Request $request) {
    #return 'Good Evening..';
    $a= [];
    $a['one'] = 'Ontru';
    $a['two'] = 'Twooo';
    $a['three'] = 'Threee';
   
    if ($request->is('admin/*')) {
        return 'WWW';
    }else{
        return 'Haii';
    }

    return response($a, 500);
    // return response()->json([
    //     'one' => 'Ontru',
    //     'two' => 'Twooo',
    //     'three' => 'Threee',
    // ]);
    #return redirect()->route('firstr');
    #return redirect(route('firstr'));
    #return redirect()->action();
    #return redirect()->away('https://laravel.com/docs/7.x/responses');
    #return 'Good Evening..';
    #return redirect('first')->with('status', 'Profile updated!');
}]);

/* 
return response('Hello World', 500)->header('Content-Type', 'application/json'); //'text/plain'

*/


Route::get('/post', function (Request $request) {

    //User::Create(['name'=>'Tig','email'=>'Tig@gmail.com','password'=>bcrypt('suresh')]);
    //  User::Create(['name'=>'boss','email'=>'boss@gmail.com','password'=>bcrypt('suresh')]);
    //  User::Create(['name'=>'don','email'=>'don@gmail.com','password'=>bcrypt('suresh')]);

    $arr = [];
    #$user = User::with('address')->get();
    $user = [];
    #$address = Address::where('country', 'India')->orWhere('id', '1')->with('account')->get();
    // $address = Address::where(function (Builder $query){
    //                 return $query->where('country', 'India')
    //                              ->orWhere('id', '1');
    //             })->get();
    // $address = Address::where('country', 'India')
    //                     ->orWhere('id', '1')->get();

    // $address = Address::where(function (Builder $query){
    //                 return $query->where('country','India')
    //                         ->where('id','1');
    //             })->account()->get();

    #$address =  Address::with('account')->get();

    // $user = User::whereHas('address', function(Builder $query){
    //     $query->where('country', 'like','%India%');
    // })->get();

    // $name = 'ram';
    // $id = '1';
    // $country = 'india';
    // $aid = '2';

    // $address = Address::where(function(Builder $qry) use($country,$aid) {
    //                 if($aid == '1') {
    //                     $qry->where('country',$country)->orWhere('id',$aid);
    //                 }else {
    //                     $qry->orWhere('id','>=','1');
    //                 }                    
    //             })
    //             ->whereHas('account', function(Builder $query) use($name, $id){
    //                 $query->where('name', $name)->orWhere('id',$id);
    //             })
    //             ->with('account')
    //             ->get();
    
    /* $user = User::whereHas('address', function(Builder $query) {
                $query->where('account_id', '1');
            },'>=',3)
            ->with('address')
            ->get(); */
   /*  $user = User::whereDoesntHave('address', function(Builder $query){
                $query->where('name','!=','ram');
                $query->where('country','India');
            })
            ->with('address')
            ->get();
            print($user); */

    // $user = User::select(['email'])->withCount(['address as address_cnt'=> function(Builder $query){
    //     $query->where('users.name','ram');
    // }])->with('address')->get();
    #print($user);

    // $first = User::where('id',2)->first();

    // $f_count = $first->loadCount('address');

    // $user = User::with(['address'=> function($query){
    //             $query->groupBy('account_id');
    //         }])->get();

    // print('<br>');
    // print($user);    
    // print('<br>');
    // print('<br>');


    // $user = User::doesntHave('address')
    //             ->with('address')
    //             ->get();
    //             print($user);

    #Address::Create(['account_id'=>$user->id, 'country'=>'India']);
    #return $address;
    #return view('user.index', [$user,$address]);
    #return view('user.index', $arr);

    $user = $address = [];

    #$user = User::select(['id','email'])->has('address')->with('address:id,country')->get();
    // $user = User::select(['id','email'])->has('address')->with(['address' => function($qry){
    //             $qry->select('id','country');
    //         }])->get();

    // $user = User::select(['id','email'])->has('address')->with(['address' => function($qry){
    //             // $qry->select('id','country');
    //             $qry->where('id','1');
    //         }])->get();

    $user = User::with('address')->select('users.id','users.email')->get();

    $address = Address::with('users')->get();

    return view('user.index', compact('user','address'));    
});


Route::get('/tag', function (Request $request) {

    $user = $address = $post = $tag = [];

    $posts = Post::first();
    #$tags = Tag::first();
    $post = Post::with('tags')->get();
    $tag = Tag::with('posts')->get();

    // $posts->tags()->attach([
    //     1=>['status'=>'Yes'],
    //     2=>['status'=>'No'],
    //     3=>['status'=>'Yes'],
    //     4=>['status'=>'No'],
    // ]);
    #$tags->posts()->attach([5]);
    #$posts->tags()->attach([6]);    
    
    return view('user.tags', compact('post','tag'));    
});


Route::get('/car', function (Request $request) {
    
    $owners = Owner::with('cars')->get();    
    $cars = Car::with(['owners','mechanics'])->get();
    $mechs = Mechanic::with('cars')->get();

    //dd($mechs);
    // Mechanic::create(['name'=>'Ravi']);
    // $car1 = Car::where('id',1)->first();
    // $car2 = Car::where('id',2)->first();
    // Owner::create(['name'=>'Mustang Owner','car_id'=> $car1->id]);
    // Owner::create(['name'=>'Ferrari Owner','car_id'=> $car2->id]);

    // $mechs = Mechanic::find(1);

    // return $mechs->carOwner;

    // $mechs = Owner::find();

    // return $mechs->carMechanic;


       
    
    return view('user.car', compact('mechs','cars','owners'));    
});


Route::get('/qqqqqqq', function (Request $request) {
    // $id =12;
    // #session(['name' => 'SURESH']);
    // $value = $request->session()->all();
    // //$value = $request->session()->all();
    // print_r('Q ---   ');
    // print_r($value); 
    // print_r('  ---  R'); exit;

    $url = url()->current();
    $url = url()->full();
    $url = url()->previous();
    return $url;
    
    // abort(404);
    // abort(403, 'Unauthorized action.');
    //$route = Route::getCurrentRoute()->getPath();
    // Log::debug('An informational message.');
    // Log::emergency('An informational emergency');
    // Log::alert('An informational alert');
    // Log::critical('An informational critical');
    // Log::error('An informational error');
    // Log::warning('An informational warning');
    // Log::notice('An informational notice');
    // Log::info('An informational info');
    // Log::debug('An informational debug');

    // Log::info('Showing user profile for user: '.$id);
    // Log::info('User failed to login.', ['id' => $id]);

    // Log::channel('slack')->info('Something happened! Apple');
    // Log::stack(['single'])->info('Something happened! QQQ');
    // Log::stack(['slack'])->info('Something happened! WWW');


    return 'hai';

    #$user2 = User::has('posts')->with('posts')->get();
    #$user = User::doesntHave('posts')->with('posts')->get();
    $user = User::where('name','kavi')->get();
    $user = User::whereIn('name',['kavi','suresh'])->get();
    $user = User::whereNotIn('name',['kavi','suresh'])->get();
    $user = User::whereIn('name',['kavi','suresh'])->orderBy('id','DESC')->get();
    $user = User::whereNotIn('name',['kavi','suresh'])->orderBy('id','DESC')->skip('2')->take('4')->get();
    $user = User::doesntHave('posts')->without('posts')->get();
    $user = User::where('name','suresh')->first();
    $user->name = 'suresshhhh';
    $user3= $user->fresh();
    User::chunk(3, function($flights){
         foreach($flights as $flight){
             echo $flight->name;
             echo "<br>";
         }
    });
    
    foreach(User::where('name','Gig')->cursor() as $flight){
        echo $flight->email;
        echo "<br>";
    }

    $user = User::where('names','Gig')->cursor()->filter(function($user){
        return $user->id > 7;
    });

    $user = User::addSelect(['qqq'=> 'name'])->get();
    $user = User::select('name','email')->addSelect(['qqq'=> User::select('name')->where('id','2')->limit(1)])->get();
    #$user = User::select('name','email')->addSelect(['qqq'=> User::select('name')->where('id','2')->count()])->get();
    $user = User::select('name')->where('id','2')->count();
    $user = User::select('name')->where('id','2')->get();
    $user = User::select('name','email')->get();
    #$user = User::select(['name','email'])->addSelect(['qqq'=> DB::raw("(select COUNT(users.id) from users as id_count")])->get();
    #$user = User::whereIn('id', function($query){
    $user = User::whereIn('id', function($query){
                $query->select('id')
                //$query->selectRaw('Min(id)')
                //$query->selectRaw('Max(id)')
                      //->from('users')
                      ->from(with(new User)->getTable())
                      //->whereIn('name',['kavi','Gig']);
                      ->whereBetween('id',['4','8'])
                      ->groupBy('name');
            })
            ->select(['id','name','email'])->get();

    $user = User::select('users.*')->get();
    $user = User::selectRaw('max(id) as mmid, min(id) as mid')->get();
    $user = User::where('name','ram')->get();
    $user = User::where(function($query){
                $query->where('name','ram');
                $query->where('id','1');
            })->get();

    $user = User::firstWhere(function($query){
                $query->where('name','ram');
                $query->where('id','1');
            });

    #$user = User::where('id',4)->findOrFail(1);
    $user = User::where('name','Gig')->count();
    $user = User::where('name','Gig')->max('id');
    $user = User::find(4);
    $user->getOriginal();
    #$user = Demo::firstOrCreate(['name' => 'Ping'],['email'=>'Ping@gmail.com','password'=>bcrypt('suresh')]);    
    #$user = User::firstOrCreate(['name' => 'king']);
    #$user = User::firstOrNew(['name' => 'Ring']);

    #$user = User::updateOrCreate(['name' => 'Ping'],['email'=>'Ping@gmail.com','password'=>bcrypt('suresh')]);

    //$user = $user->isDirty(); ////isDirty('name')
    //$user = $user->isClean(); //isClean('name')
    //$user = $user->wasChanged(); //wasChanged('name')
    
    //$user = User::where('id','10')->findOrFail(['id','name','email']);

    #$user = User::chunk(3)->get();
    #$user->refresh();

    #$user = Demo::firstOrCreate(['title' => 'Demo2'],['status'=>'Yes']);

    //$user = Demo::destroy(collect([1,2,3]));
    // $user = Demo::onlyTrashed()->get();
    // $user = Demo::onlyTrashed()->get();

    // $user = Demo::withTrashed()->restore();
    //$user = Demo::onlyTrashed()->restore();
    //$user->history()->restore();

    #$qqq= $user->history()->withTrashed()->get();
    #$user->delete(); 


    // $user = Demo::create(['title' => 'Demo6','status'=>'Yes']);
    // $qqq = $user->replicate()->fill(['title' => 'Demo8']);

    //$user = Demo::all();
    // $user = $user->contains(1);
    // $user = $user->diff(Demo::whereIn('id',[2,3,4])->get());
    // $user = $user->except([1,2,3]);
    // $user = $user->intersect (Demo::whereIn('id',[2,3,4])->get());
    // dd($user);
    // $user = $user->reject(function($query){
    //     return $query->status === 'Yes';
    // })->map(function($query){
    //     return $query->status;
    // });

    //$user = Mechanic::doesntHave('cars.owners')->with('cars')->get();
    //$user = Mechanic::has('cars.owners')->with('cars')->get();
    //$user = Mechanic::with('cars')->get();
    //$user = Mechanic::has('cars')->with('cars.owners')->get(); // ## SUPER
    //$mech = Mechanic::all();
    //$user = $mech->load(['cars.owners']);

    //$mech = User::all();
    //$user = Car::all();
    //$user = $mech->load(['cars.owners']);
    //$user = $user->loadMissing(['mechanics','owners']);
    //$user = $mech->modelKeys();
    //$user = $mech->makeVisible('name');
    //$user = $mech->makeHidden('name');
    //$user = $mech->only ([1,2,3]);
    #return view('user.others');
    // $user = Demo::whereIn('title',['Demo1','Demo2'])->get();
    // $user = $user->toQuery()->update(['title'=> 'DemoA']);
   // $user = User::find(1);
    //$user = $user->toArray();
    //$user = $user->attributesToArray();
    //$user = $user->toJson(JSON_PRETTY_PRINT);
    //$user = $user->unique('name');


     
    //$q= ["name"=>"GFG", "email"=>"abc@gfg.com"]; 
    //$user = Demo::create(['title'=>'Demo 14', 'status'=>'Good']);
    //$user = Demo::find(12);

    // $jsonobj = '{"Peter":35,"Ben":37,"Joe":43}';
    // $q= json_decode($jsonobj, true);

    //dd($user);

    // $user = User::select(['id','name','email'])->addSelect([
    //     'post_id'=> Post::select('title')->whereColumn('user_id','users.id')->orderBy('id','DESC')->limit(1),        
    //     'post_count'=> Post::selectRaw('COUNT(id)')->whereColumn('user_id','users.id'),
    //     'cr_date'=> Post::select('created_at')->whereColumn('user_id','users.id')->orderBy('id','DESC')->limit(1),
    //     'test'=> Post::select('title')->limit(1),
    //     ])->withCasts(['cr_date' => 'datetime:d/m/Y H','tests'=>'array'])->get();

    //$user = User::with('posts:title,user_id,updated_at')->get();
    #$user = Mechanic::with(['cars.owners:car_id,name','cars:mechanic_id,model',])->get();
    #$user = Mechanic::with(['cars'=> function($qry){$qry->select('mechanic_id','model');},'cars.owners'=> function($qry){$qry->select('car_id','name');}])->get();

    // $user = Mechanic::with(['cars:mechanic_id','model'=> function($qry){     
    //             $qry->with(['cars.owners'=> function($qry){     
    //                     $qry->select('car_id','name');
    //                 }]);
    //         }])
    //         ->get();

    //$user =  User::find(1)->posts()->where(function($qry){$qry->where('title','Post 2');})->get();

    #$user = $user->posts()->get();
   
 
    return $user;


    #$post = Post::all();
    #$post = Post::with('tags:name')->find(1);    
    #$post = $post->tags()->get();
    
    // $post->movideos()->create(['title'=>'Video 4']);
    // dd($post->movideos);
    //dd($post->tags);
    #$post = Post::where('id',4)->first();
   # $video = Video::find(1);
    #$tag = Tag::find(4);

    #$post->motags()->attach($tag);
    #$video->motags()->attach($tag);
    //$post->tags()->attach([$tag->id=>['status'=>'Yes']]);
        //     'name'=>'React',
        //     'status' =>
        // ]);

    // $post->tags()->create([
    //     'name'=>'React',
    //     'status' =>
    // ]);
    #$user = Comment::find(3);

    //dd($user->comments);
    // $post = Post::find(2);

    // $v1 = Video::create(['title'=> 'Video1']);
    // $v2 = Video::create(['title'=> 'Video2']);
    
    // $v1->comments()->create([
    //     'user_id'=>$user->id,
    //     'body'=> 'First Video Comment..',
    // ]);
    // $v2->comments()->create([
    //     'user_id'=>$user->id,
    //     'body'=> 'Second Video Comment..',
    // ]);
    
    return 'Good';    
});

Route::get('user/{id}/profile', function ($id) {
    //
})->name('profile');

Route::get('user/hai', function () {
    // $url = url('profile', ['id' => 1]);
    // print_r($url);
    // return $url;
    $route = Route::current();
    print_r($route);
    #return redirect()->route('profile', ['id' => 1]);
})->name('qqq');

Route::get('aii/users/{user}', function (App\User $user) {
    return $user->email;
});

Route::get('aii/sgsds/{abc}', function (App\User $abc) {
    return $abc->email;
});

Route::get('ai/posts/{post:title}', function (App\Post $post) {
    return $post;
});

Route::get('abc/users/{user_id}/aaa/{post:title}', function (User $user_id, Post $post) {
    return $post;
});

/* 
$user = User::with('address')->get();
$address = Address::with('account')->where('country', 'India')->get();
$address = Address::with('account')->where('country', 'India')->orWhere('id', '1')->get();

$user = User::whereHas('address', function(Builder $query) use($country, $id){
                return $query->where('country', 'like', $country)->orWhere('id',$id);
            })->get();

$name = 'ram';
    $id = '1';
    $country = 'india';
    $aid = '2';

    $address =  Address::where(function(Builder $qry) use($country,$aid) {
                    if($aid == '1') {
                        $qry->where('country',$country)->orWhere('id',$aid);
                    }else {
                        $qry->orWhere('id','1');
                    }                    
                })
                ->whereHas('account', function(Builder $query) use($name, $id){
                    $query->where('name', $name)->orWhere('id',$id);
                })
                ->with('account')
                ->get();
    
    $user = User::whereHas('address', function(Builder $query) {
                $query->where('account_id', '1');
            },'>=',3)
            ->with('address')
            ->get();

    $user = User::whereDoesntHave('address', function(Builder $query){
                $query->where('name','!=','ram');
                $query->where('country','India');
            })
            ->with('address')
            ->get();
            print($user);

    $user = User::withCount('address as address_cnt')->with('address')->get();

    $user = User::withCount(['address as address_cnt'=> function(Builder $query){
        $query->where('account_id',1)->where('users.name','ram');
    }])->with('address')->get();
    print($user);

    $user = User::doesntHave('addresses')->with('addresses')->select(['id','email'])->get();
    $user = User::has('addresses')->with('addresses')->select(['id','email'])->get();

     $user = User::select([
        'id',
        'name',
        'email',
        'post_id'=> Post::select('title')->whereColumn('user_id','users.id')->limit(1)
        ])->get();

    $user = User::select(['id','name','email'])->addSelect([
        'post_id'=> Post::select('title')->whereColumn('user_id','users.id')->limit(1)
        ])->get();

    $user = User::select(['id','name','email'])->addSelect([
        'post_id'=> Post::select('title')->whereColumn('user_id','users.id')->orderBy('id','DESC')->limit(1),
        'cr_date'=> Post::select('created_at')->whereColumn('user_id','users.id')->orderBy('id','DESC')->limit(1)
        ])->withCasts(['cr_date' => 'datetime:d/m/Y H'])->get();

    $user = User::select(['id','name','email'])->addSelect([
        'post_id'=> Post::select('title')->whereColumn('user_id','users.id')->orderBy('id','DESC')->limit(1),        
        'post_count'=> Post::selectRaw('COUNT(id)')->whereColumn('user_id','users.id'),
        'cr_date'=> Post::select('created_at')->whereColumn('user_id','users.id')->orderBy('id','DESC')->limit(1),
        'test'=> Post::select('title')->limit(1),
        ])->withCasts(['cr_date' => 'datetime:d/m/Y H','tests'=>'array'])->get();

    User::where('users.name','ram')->with(['posts'=> 
                    function($qry){ 
                        $qry->where('title','Post 2');
                    }])->get();
                    
 

*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::any('/upload', 'TestController@upload')->name('upload');

Route::get('/setcookie', function (Request $request) {
    Cookie::queue('mycookie2','EEE',0);
    Cookie::queue('mycookie4','RRR',5);
    // if(Cookie::queue('mycookie','One',5)){
    //     return 'Cookie setted succefully';
    // }
    return 'Cookie Not set';
});
Route::get('/getcookie', function (Request $request) {   
    #return $request->cookie('mycookie');
    #return [Cookie::get('mycookie2'),Cookie::get('mycookie3'),Cookie::get('mycookie4')];
   # return [Cookie::get('mycookie2'),Cookie::get('mycookie3'),Cookie::get('mycookie4')];

    $cookie = Cookie::forget('mycookie4');

    #return Cookie::get('laravel_session'); //laravel_session

    // get the encrypter service
    // $encrypter = app(\Illuminate\Contracts\Encryption\Encrypter::class);

    // // // decrypt
    // $decryptedString = $encrypter->decrypt(Cookie::get('laravel_session'));
    // dd($decryptedString);

    $value = Crypt::encrypt(Cookie::get('laravel_session'));
    // eyJpdiI6ImVoNEVlVWpnYUdwZ1JHRlJWSGlTZEE9PSIsInZhbHVlIjoiVThpWjJNWVBqZnVsWjhLVWNDXC85VHc9PSIsIm1hYyI6IjFjMDRhOTM5ZThhOWRmYjk3Mzk0OWFmNTM3YWE1NDAzNzMxNWY5YTJmODMwNmQxZDE4NDllZGJkMjc1Y2I3ZmYifQ==
    base64_decode($value);
    print_r(base64_decode($value));
    // {"iv":"eh4EeUjgaGpgRG
    return 'fghf';
});

Route::get('/setsession', function (Request $request) {
    session()->getId();
    return [session()->getId()];
});

Route::get('/getsession', function (Request $request) {
    $q= session()->getId();
    $q= session()->setId($q,true);
    //session()->regenerate();
    $e= session()->getId();
    return [$q,$e];
});

Route::get('/response', function (Request $request) {    
    $status = 500;
    $arr = [];
    $headers = [
        'Content-Type' => 'text/plain',
        'X-Header-One' => 'Header Value',
        'X-Header-Two' => 'Header Value'
    ];
    $arr['name']='suresh';
    $arr['email']='suresh@gmail.com';
    $arr['mobile']='996923923';
    #return response($arr, $status, $headers);
    return response()->json(['msg' => "Session timed out", 'status' => 401, 'redirect_url' => 'sdadfs'], 401);
});

Route::get('/pdf', function (Request $request) {    
    #$html = '<h1>Hello Man...dsssa</h1>';
    #$pdf = PDF::loadHtml($html);
    $data = [
        'owner'=>'Kumarn',
        'car'=>'Ford Mustang',
        'mechanic'=>'Mr MC1',
    ];
    #return view('user.pdf.test',$data);
    $pdf = PDF::loadView('user.pdf.test',$data);
    $pdf->setPaper('a4')->setOrientation('landscape')->setOption('margin-bottom', 0);
    #$pdf->setOption('footer-html', view('user.pdf.footer'));
    return $pdf->stream();
    #return $pdf->download('test_pdf.pdf');
});

#doc print | export | csv | pdf
Route::get('/doc/{type}', 'TestController@export')->name('export'); 

Route::get('/curl', 'TestController@curl')->name('curl'); 
Route::get('/user-list', 'TestController@userList')->name('userList'); 


Route::get('/bcrypt', function(Request $request){

    $q = bcrypt('12345678');
    $w = password_hash('12345678', PASSWORD_DEFAULT);
    echo $q;
    echo "<br>";
    echo $w;
    echo "<br>";
    echo "<br>";

    #if (password_verify('12345678', $q)) {
    if (password_verify('12345678', $q) && password_verify('12345678', $w)) {
        return 'Password is valid!';
    } else {
        return 'Invalid password.';
    }
    return $q;
}); 

Route::get('/aes/encrypt', function(Request $request){
    
    $plainText = 'Surrai Pootru';
    $cipher = 'AES-256-ECB';
    $key = 'indianactorsaravananindianactorsaravanan';
    $initVector = '';

    $res = openssl_encrypt(
        $plainText,
        $cipher,
        $key,
        OPENSSL_RAW_DATA,
        $initVector
    );
    $result = base64_encode($res);
    return $result;  
}); 

Route::get('/aes/decrypt', function(Request $request){

    $plainText = base64_decode('bLkVlBMs1crt796auUdIHg==');

    //print($plainText); exit;

    $cipher = 'AES-256-ECB';
    $key = 'indianactorsaravananindianactorsaravanan';
    $initVector = '';

    $length = strlen($key);
    if($length == 16 || $length == 24 || $length == 32)
    {
        return 'Invalid key';
    }   
    
    $res = decrypt(
        $plainText,
        $cipher,
        $key,
        OPENSSL_RAW_DATA,
        $initVector
    );
    return $res;  
}); 


use Illuminate\Contracts\Encryption\Encrypter as EncrypterContract;

// Route::get('/aes/generateKey', function(Request $request){ 
//     #$cipher = EncrypterContract::generateKey('AES-128-CBC');
//     #return $cipher;  
//     return $cipher;  
// }); 

use App\Providers\Lib\Test;

Route::get('/provider', function(Request $request) { 
    return Test::$name;    
    #return Test::Testing();
}); 

Route::get('profile', function () {
    // Only authenticated users may enter...
})->middleware('auth.basic');

//@PAYMENTGATEWAY
Route::get('payment-initiate', 'PaymentController@paymentInitiate')->name('payment_initiate');
Route::any('payment-initiate-req', 'PaymentController@paymentInitiateReq')->name('payment_initiate_req');

Route::any('first', 'TestController@first')->name('first');

Route::post('datatable', 'TestController@datatable')->name('datatable');

Route::get('demo', 'TestController@demo')->name('demo');
Route::get('demo2', 'TestController@demo2')->name('demo2');