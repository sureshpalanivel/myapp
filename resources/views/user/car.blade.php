
@extends('main')

@section('title', 'Tags')

@section('sidebar')    
    <h1> We are from Serviceprovider boot function </h1>
    <h3>{{$iam}} </h3>
    <h3>{{$anotherone}} </h3>
@endsection

@section('content')
    <h1>OWNERS</h1>
    @foreach($owners as $own)
    <tr>
       <td>
        <h2>{{$own->name}}</h2>
        <p>Car - {{$own->cars->model}}</p>
        <p>Mechanic - {{optional($own->carMechanic)->name}}</p>
       </td>
    </tr>
    @endforeach 
    <hr><br/>

    <h1>CARS</h1>
    @foreach($cars as $car)
    <tr>
       <td>
        <h2>{{$car->model}}</h2>
        <p>Owner - {{$car->owners->name}}</p>
        <p>Mechanic - {{$car->mechanics->name}}</p>
       </td>
    </tr>
    @endforeach 
    <hr><br/>

    <h1>MECHANICS</h1>
    @foreach($mechs as $mec)
    <tr>
       <td>
            <h2>{{$mec->name}}</h2>   
            <p>Car - {{$mec->cars->model}}</p>
            <p>Owner - {{$mec->carOwner->name}}</p>
       </td>
    </tr>
    @endforeach
    <hr><br/>
    
@endsection