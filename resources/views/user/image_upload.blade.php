
@extends('main')

@section('title', 'Image Upload')

@section('content')
    <h3>Image Upload...</h3>
    @auth
      <img src="{{asset('storage/images/'.Auth::user()->picture)}}" width="200" height="200"/>
    @else
      <p>No Image Found..</p>
    @endauth

    <x-img-alert>
       <p>Hai I am alert...</p>
    </x-img-alert>

    @if($errors->any())
    {{print_r($errors->all())}}
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $k=>$v)
                   <li>{{$k}} - {{$v}}</li>
                @endforeach
            </ul>
        </div>
    @endif
   
    <h3>AAAAAA</h3>

    @error('picture')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <form name="image_upload" method="post" action="{{route('upload')}}" enctype="multipart/form-data">
        @csrf
        Select image to upload:
        <input type="file" name="picture" id="picture">
        <input type="submit" value="Upload Image">
    </form>

@endsection