
@extends('main')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@endsection

@section('content')

    <pre>
        {{$user}}
        <hr><br>
        {{$address}}
    </pre>

    <p>TABLE 1</p>
    <table>
    @foreach($user as $a)
    <tr>
       <td>{{$a->email}} 
       [
          {{$a->address->country}} 
       ]
       ({{$a->address_cnt}})</td>
    </tr>
    @endforeach
    </table>

    <br><hr>
    <p>TABLE 2</p>
    <table>
    @foreach($address as $b)
    <tr>
       <td>{{$b->country}} 
       [
        {{$b->users->email}}     
       ]
       </td>
    </tr>
    @endforeach
    </table>

@endsection