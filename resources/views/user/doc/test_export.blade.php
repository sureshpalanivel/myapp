<div class="rightbox">
    <div class="homeMsg" style="text-align:left; height:auto;"> 
        <table id="example1" border="1" class="table table-striped">
            <thead>
                <tr>
                  <th colspan='3'><b>Car Details - <?php echo date("d-M-Y"); ?></b></th>
                </tr>               
                <tr>
                    <th>Car</th>
                    <th>Owner</th>
                    <th>Mechanic</th>
                </tr>              
            </thead>
            <tbody>               
                @if (isset($excel) && $excel != '')              
                    @foreach($excel as $row)
                        <tr>
                            <td class="text-left">{{$row['car']}}</td>
                            <td class="text-left">{{$row['owner']}}</td>
                            <td class="text-left">{{$row['mechanic']}}</td>
                        </tr>
                    @endforeach               
                @else
                    <tr><td colspan='9'>No Records Found.</td></tr>
                @endif
            </tbody>
        </table>
    </div>
</div>