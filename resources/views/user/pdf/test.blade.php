@extends('main')

@section('title', 'Tags')

@section('sidebar')    
    <h1>HasOneThrough </h1>
@endsection

@section('content')
    <style>

    body {background-color: powderblue;}
    h1   {color: blue;}
    p    {color: red;}

    </style>

    <h1>OWNERS</h1>
    <p>{{$owner}}</p>
    <hr><br/>

    <h1>CARS</h1>
    <p>{{$car}}</p>
    <hr><br/>

    <h1>MECHANICS</h1>
    <p>{{$mechanic}}</p>
    <hr><br/>    
    
@endsection
    