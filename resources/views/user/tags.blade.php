
@extends('main')

@section('title', 'Tags')

@section('sidebar')
    @parent   
@endsection

@section('content')
    @foreach($post as $ps)
    <tr>
       <td>
       <h2>{{$ps->title}} ({{$ps->user->name}})</h2>
            <ul>
                @foreach($ps->tags as $tags)
                   <li>{{$tags->name}}  -  {{$tags->posttag->created_at}} Status - {{$tags->posttag->status}}</li> 
                @endforeach
           </ul>
       </td>
    </tr>
    @endforeach 
    <hr><br/>
    @foreach($tag as $tg)
    <tr>
       <td>
       <h2>{{$tg->name}}</h2> 
            <ul>
                @foreach($tg->posts as $post)
                   <li>{{$post->title}}  -  {{$post->pivot->created_at}}</li> 
                @endforeach
           </ul>
       </td>
    </tr>
    @endforeach   
@endsection