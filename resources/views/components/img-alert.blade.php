<div>
    <!-- Simplicity is an acquired taste. - Katharine Gerould -->
    {{$slot}}
    @if(session()->has('success'))
        @if(session()->has('good'))
           <h1 class="alert alert-success">{{session()->get('good')}}</h1>
        @endif
       <div class="alert alert-success">{{session()->get('success')}}</div>
    @elseif(session()->has('error'))
       <div class="alert alert-waring">{{session()->get('error')}}</div>
    @endif
</div>