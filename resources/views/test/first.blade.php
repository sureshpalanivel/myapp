@extends('test.master')

@push('stylee')
<style>
.color {
    color: blue;
}
</style>
@endpush

@prepend('stylee')
<style>
.color {
    color: green;
}
</style>
@endprepend

@section('sidebar')
   @parent
<div class="container">
    <h3 class="color">SIDE BAR</h3>
</div>
@endsection

@section('content')
<div class="container">
    <h3 class="color">FIRST</h3>

    <div class="">
        <table id="example" class="display" style="width:100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                </tr>
            </thead>
            {{--  <tbody>
            </tbody>  --}}
            <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                </tr>
            </tfoot>
        </table>
    </div>

    <form action="{{route('first')}}" id="testForm" name="testForm">  
        @csrf     
        <div class="form-row">
            <div class="form-group col-md-6">
            <label for="inputEmail4">Email</label>
            <input type="email" name="email" class="form-control" id="inputEmail4" placeholder="Email">
            </div>
            <div class="form-group col-md-6">
            <label for="inputPassword4">Password</label>
            <input type="password" name="password" class="form-control" id="inputPassword4" placeholder="Password">
            </div>
        </div>
        <div class="form-group">
            <label for="inputAddress">Address</label>
            <input type="text" name="address1" class="form-control" id="inputAddress"  value="aaaa" placeholder="1234 Main St">
        </div>
        <div class="form-group">
            <label for="inputAddress2">Address 2</label>
            <input type="text" name="address2" class="form-control" id="inputAddress2" value="bbbb" placeholder="Apartment, studio, or floor">
        </div>
        <div class="form-row">
            <div class="form-group col-md-6">
            <label for="inputCity">City</label>
            <input type="text" name="city" class="form-control" value="dddd" id="inputCity">
            </div>
            <div class="form-group col-md-4">
            <label for="inputState">State</label>
            <select id="inputState" name="state" class="form-control">
                <option selected>Choose...</option>
                <option>...</option>
            </select>
            </div>
            <div class="form-group col-md-2">
            <label for="inputZip">Zip</label>
            <input type="text" name="zip" class="form-control" value="cccc" id="inputZip">
            </div>
        </div>
        <div class="form-group">
            <div class="form-check">
            <input name="email" class="form-check-input" type="checkbox" id="gridCheck">
            <label class="form-check-label" for="gridCheck">
                Check me out
            </label>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Sign in</button>
    </form>
</div>
@endsection

@push('scriptee')
<script>
//console.log('AAAAAAA')
</script>
<script type="text/javascript" src="{{asset('assets/js/test/first.js')}}"/>
@endpush

@prepend('scriptee')
<script>
//console.log('BBBBBB')
</script>
@endprepend

