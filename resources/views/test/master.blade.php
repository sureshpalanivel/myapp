
<html>
    <head>
        <title>App Name - @yield('title')</title>
        <meta name="csrf-token" content="{{csrf_token()}}">

        {{--  <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>       
        <!-- Popper JS -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>  --}}

        <link rel="stylesheet" type="text/css" href="{{asset('assets/theme/bootstrap/4.4.1/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
        {{--  <script type="text/javascript" src="{{asset('assets/theme/jquery/3.5.1/jquery.min.js')}}"></script>  --}}
        <script type="text/javascript" src="{{asset('assets/theme/jquery/3.5.1/jquery.min.js')}}"></script>
        {{--  <script type="text/javascript" src="{{asset('assets/theme/jquery/3.5.1/popper.min.js')}}"></script>  --}}
        <script type="text/javascript" src="{{asset('assets/theme/jquery/jquery.validate.min.js')}}"></script>  
        <script type="text/javascript" src="{{asset('assets/theme/bootstrap/4.4.1/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

        {{--  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="{{asset('assets/theme/bootstrap/4.4.1/css/bootstrap.min.css')}}">
        <script type="text/javascript" src="{{asset('assets/theme/jquery/3.5.1/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/theme/jquery/3.5.1/popper.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/theme/bootstrap/4.4.1/js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('assets/theme/jquery/jquery.validate.min.js')}}"></script>          --}}

        <style>
        .error{
            color:red;
        }
        </style>
        @stack('stylee')       
    </head>
    <body>
        @section('sidebar')
            <h3>This is the master sidebar.</h3>
        @show

        <div class="container">
            @yield('content')
        </div>
        <script type="text/javascript" src="{{asset('assets/js/test/main.js')}}"></script>
        @stack('scriptee')
    </body>
</html>