<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //
    protected $fillable = [
        'account_id', 'country',
    ];

    public function account()
    {
        return $this->belongsTo(User::class);
        #return $this->belongsTo(User::class, 'account_id', 'id');
    }

    public function users()
    {
        return $this->belongsTo(User::class, 'account_id', 'id');
    }    
}
