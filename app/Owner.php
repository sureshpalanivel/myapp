<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    //
    protected $fillable = [
        'name', 'car_id',
    ];

    public function cars()
    {
        return $this->belongsTo(Car::class, 'car_id', 'id');
    }

    public function carMechanic()
    {
        return $this->hasOneThrough(Mechanic::class, Car::class, 'mechanic_id', 'id', 'id', 'id')->withDefault(['name' =>'No Cars and Owners Available']);
    }
}
