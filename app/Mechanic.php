<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mechanic extends Model
{
    //
    protected $fillable = [
        'name',
    ];

    public function cars()
    {
        #return $this->hasOne(Car::class, 'mechanic_id', 'id');
        return $this->hasOne(Car::class, 'mechanic_id', 'id')->withDefault(['model' =>'No Cars']);
    }

    public function carOwner()
    {
        // return $this->hasOneThrough(Owner::class, Car::class, 'mechanic_id', 'car_id', 'id','id');
        return $this->hasOneThrough(Owner::class, Car::class, 'mechanic_id', 'car_id', 'id','id')->withDefault(['name' =>'No Cars and Owners Available']);
        // return $this->hasOneThrough(Car::class, 'mechanic_id', 'id')->withDefault(['model' =>'No Cars']);
    }
}
