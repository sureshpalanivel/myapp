<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //

    protected $fillable = [
        'user_id', 'title',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function tags()
    {
        #return $this->belongsToMany(Tag::class)
        //->using(PostTag::class) // Cutome intermediate table name alias for 'post_tag' extent Pivot model is manditory
        return $this->belongsToMany(Tag::class, 'post_tag', 'post_id', 'tag_id')    
        ->as('posttag') // Alisa name of pivot
        ->withPivot(['status']) // Take extra columns
        ->withTimestamps(); // Pivot table not added automatially timestamps
        // ->wherePivot('status','Yes') 
        // ->wherePivotIn('status',['Yes','No']) 
        // ->wherePivotNotIn('status',['Yes','No'])       
    }

    public function comments()  //return collections
    {
        return $this->morphMany(Comment::class, 'commentable')->latest();      
    }

    public function comment()  //return single
    {
        return $this->morphOne(Comment::class, 'commentable')->latest();      
    }

    public function motags()  //return single
    {
        return $this->morphToMany(Tag::class, 'taggable');    
        //## May be table name is diffrent Mention table name in third parameter (Without tbl name fiel check try add s in second parameter "taggables")
        //return $this->morphToMany(Tag::class, 'taggable','table name');         
    }
}
