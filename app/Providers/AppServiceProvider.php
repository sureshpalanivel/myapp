<?php

namespace App\Providers;

#use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

use View;
use App\Car;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
       
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        // Relation::morphMap([
        //     'Post' => \App\Post::class,
        //     'Video' => \App\Video::class,
        // ]);

        View::share('iam','I am from Service Provider BOOT'); // Available All Place 
        View::composer('user.car',function($abc){ // Only Available 
            $car = Car::all();
            $abc->with([
                'anotherone'=> 'I am also from Service Provider BOOT Function',
                'carlist'=> $car,
            ]);
        });
    }
}
