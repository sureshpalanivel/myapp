<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Casts\Json;

class Demo extends Model
{
    //
    use SoftDeletes;
    
    //protected $dateFormat = 'Y-m-d H:i:s'; //Default time format

    protected $fillable = [
        'title', 'status',
    ];

    protected $casts = [
        //'status' => 'array', //deserialize the serilized data after it stored
        //'status' => 'boolean', Return True the field is not empty otherwise False 
        //'status' => Json::class,
        //'created_at' => 'datetime:Y-m-d',
        //'created_at' => 'date:Y-m-d',
    ];

    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = strtoupper($value);
    }
}
