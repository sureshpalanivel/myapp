<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    //
    protected $fillable = [
        'name','mobile','email'
    ];

    public function details()
    {
       return $this->hasOne(StudentDetails::class, 'student_id', 'id');
    }

    public function detailses()
    {
       return $this->hasMany(StudentDetails::class, 'student_id', 'id');
    }
}
