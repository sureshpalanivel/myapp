<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Illuminate\Support\Facades\Storage;

use Laravel\Passport\HasApiTokens; //Passport

#use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable; ////Passport
    // use Notifiable;
    #use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','picture','str','int','country','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];   

    public static function boot()
    {
        parent::boot();
       
        static::creating(function($item){
            //dd('Creating', $item);
        });

        static::created(function($item){
            //dd('Created', $item);
        });
    }
    
    public function address()
    {
        #return $this->hasOne(Address::class, 'account_id', 'id')->withDefault(['country'=>'No COuntry']);
        return $this->hasOne(Address::class, 'account_id', 'id')->withDefault(function($abc){
            if(in_array($abc->account_id,[4,5])){
                $abc->country = 'No Country';
            }else{
                $abc->country = 'My Country';
            }            
            #$abc->country = 'No COuntry';
        });
    }

    public function addresses()
    {
        return $this->hasMany(Address::class, 'account_id', 'id');
    }

    public function posts()
    {
        return $this->hasMany(Post::class, 'user_id', 'id');
    }

    public function getNameAttribute($value)
    {
        #return strtoupper($value);
        return ucfirst($value);
    }

    public function getCreatedAtAttribute($value)
    {
        $date=date_create($value);
        return date_format($date,"Y/m/d H:i:s a");
    }

    public function getUpdatedAtAttribute($value)
    {
        $date=date_create($value);
        return date_format($date,"Y/m/d H:i:s A");
    }

    public function getFullNameAttribute()
    {
        return "{$this->name} {$this->name}";
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucfirst($value);
    }   

    public function setPasswordAttribute($password)
    {
        return $this->attributes['password'] = bcrypt($password);
    }

    public static function uploadImage($picture, $user= null)
    {
        $file_name = $picture->getClientOriginalName();         
        if(!is_null(auth()->user()))
        {
            (new self)->deleteOldImage();
            $picture->storeAs('images',$file_name,'public');
            auth()->user()->update(['picture'=> $file_name]);  
        }elseif(!is_null($user)) {
            $picture->storeAs('images',$file_name,'public');
            $user->update(['picture'=> $file_name]);
        }       
    }
    
    public function deleteOldImage()
    {
        if(auth()->user()->picture){
            Storage::delete('/public/images/'.auth()->user()->picture);
        }
    }

}
