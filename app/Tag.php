<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //

    protected $fillable = [
        'name',
    ];

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'post_tag', 'tag_id', 'post_id')->withPivot(['status'])->withTimestamps();
    }

    public function moposts()
    {
        return $this->morphedByMany(Post::class, 'taggable');
        #return $this->morphedByMany(Post::class, 'taggable', 'taggables');
    }

    public function movideos()
    {
        return $this->morphedByMany(Video::class, 'taggable');
        #return $this->morphedByMany(Video::class, 'taggable', 'taggables');
    }
}
