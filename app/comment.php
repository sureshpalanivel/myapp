<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class comment extends Model
{
    //
    protected $guarded = [];
    // public function commentable()
    // {
    //     return $this->morphTo();      
    // }
    public function subject() 
    {
        return $this->morphTo('commentable');      
    }
}
