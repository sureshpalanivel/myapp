<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Builder;

use App\User;
use App\Address;
use App\Post;
use App\Tag;
use App\Mechanic;
use App\Car;
use App\Owner;
use App\Video;
use App\Comment;
use App\Demo;
use App\Student;
use App\StudentDetails;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\Http\Request\UploadRequest;
use PhpParser\Node\NullableType;
use Illuminate\Database\Eloquent\Relations\MorphTo;


class TestController extends Controller
{

    public function upload(Request $request) 
    {               
        //dd(Auth::user());
        //dd(Auth::id());
        //dd(auth()->user());
        //dd($request->all());
        if($request->isMethod('POST') )
        {
            // if($request->hasFile('picture')) 
            // {   
               /*  $validator = $request->validate([
                    'picture'=>'required|file|mimetypes:image/jpg,image/jpeg,image/png|max:2048'
                ]); */

                $rules = [
                    'picture'=>'required|file|mimetypes:image/jpg,image/jpeg,image/png|max:2048'
                ];
                $messages = [
                    'picture.required'=>'Picture is required',
                    'picture.file'=>'File is invalid',
                    'picture.mimetypes'=>'Mimetypes invalid',
                    'picture.max'=>'Maximum invalid'
                ];

                $validator = Validator::make($request->all(), $rules, $messages);
        
                if ($validator->fails()) {
                    return redirect()->back()
                                ->withErrors($validator)
                                ->withInput();
                }
                print($validator); exit;

                //User::uploadImage($request->picture);
                #session()->flash('success','Image is uploaded successfully..');
                #session()->put('success','Image is uploade successfully..');
                return redirect()->back()->with(['success'=>'Image is uploaded successfully..','good'=>'Super']);     
            // }else {
            //     return redirect()->back()->with('error','Image is Not uploaded..'); 
            // }
        }
        return view('user.image_upload');     
    }

    public function export(Request $request, $type) 
    {               
        $data =[];
        $data['excel'] = [
            0=>[
                'owner'=>'Kumarn',
                'car'=>'Ford Mustang',
                'mechanic'=>'Mr MC1',
            ],
            1=>[
                'owner'=>'Raja',
                'car'=>'Carnival',
                'mechanic'=>'Mr MC2',
            ],
            2=>[
                'owner'=>'Aswin',
                'car'=>'Rainbow',
                'mechanic'=>'Mr MC3',
            ]           
        ];
        if($type == 'export')
        {       
            $output = view('user.doc.test_export', $data);
            $headers = array(
                'Pragma' => 'public',
                'Expires' => 'public',
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'Cache-Control' => 'private',
                'Content-Disposition' => 'attachment; filename=Car_details.xls',
                'Content-Transfer-Encoding' => ' binary'
            );        
            return response($output, 200, $headers);
        }
        elseif ($type == 'print'){
            return view('user.doc.test_print', $data);    
        }
        elseif ($type == 'csvv'){ 
            $filename ='CarDetails';
            $columns = [
                ['title' => 'Car', 'name' => 'car', 'align' => 'center'],
                ['title' => 'Owner', 'name' => 'owner', 'align' => 'right'],
                ['title' => 'Mechanic', 'name' => 'mechanic', 'align' => 'center'],
            ];       

            // header('Content-Type: application/csv');
            // header('Content-Disposition: attachment; filename='.$filename.'-'.date('dMYHis').'.csv');
            $title[] = $keys = $vals = [];
        
            foreach ($columns as $c)
            {
                array_push($keys, $c['name']);
                array_push($vals, $c['title']);
            }

            print([$keys, $vals]); exit;

            $title[0] = array_combine($keys, $vals);
            $data = array_merge($title, $data);
            $f = fopen('php://output', 'w');
            foreach ($data as $d)
            {
                $d = is_array($d) ? $d : (array) $d;
                $line = [];
                foreach ($keys as $k)
                {
                    $line[$k] = $d[$k];
                }
                fputcsv($f, $line, $delimiter);
            }
            // fseek($f, 0);
            fclose($f);
            return fpassthru($f);   
            return 'asa';
            #return $this->exportCSV($filename, $columns, $data);
        }elseif ($type == 'csv'){ 
            $row = ['a'=>'qqq', 'b'=>'www', 'c'=>'eee', 'e'=>'rrr', 'f'=>'ttt'];
            // Creates a new csv file and store it in tmp directory
            $new_csv = fopen('/report.csv', 'w');
            fputcsv($new_csv, $row);
            fclose($new_csv);

            // output headers so that the file is downloaded rather than displayed
            header("Content-type: text/csv");
            header("Content-disposition: attachment; filename = report.csv");
            #return readfile("php://output/report.csv");         
            
            return fpassthru($new_csv);
        }
    }

    public function exportCSV($filename, array $columns = array(), array $data = array(), $delimiter = ',')
    {
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename='.$filename.'-'.date('dMYHis').'.csv');
        $title[] = $keys = $vals = [];
       
        foreach ($columns as $c)
        {
            array_push($keys, $c['name']);
            array_push($vals, $c['title']);
        }

        print('afa'); exit;

        // $title[0] = array_combine($keys, $vals);
        // $data = array_merge($title, $data);
        // $f = fopen('php://output', 'w');
        // foreach ($data as $d)
        // {
        //     $d = is_array($d) ? $d : (array) $d;
        //     $line = [];
        //     foreach ($keys as $k)
        //     {
        //         $line[$k] = $d[$k];
        //     }
        //     fputcsv($f, $line, $delimiter);
        // }
        // // fseek($f, 0);
        // fclose($f);
        // return fpassthru($f);
    }

    public function userList(Request $request) 
    {               
        #print('afa'); exit;
        $data = User::all();
        return response($data,200);     
    }

    public function curlllllll(Request $request)
    {
        $handle = curl_init();
 
        $url = "http://dummy.restapiexample.com/api/v1/employee";
        
        // Set the url
        //curl_setopt($handle, CURLOPT_URL, $url);
        // Set the result output to be a string.
        //curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt_array($handle, [
            CURLOPT_URL => $url,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_RETURNTRANSFER => true,
        ]);
        
        $output = curl_exec($handle);
        $res = json_decode($output);
        //print_r($res); exit;
        
        curl_close($handle);
        #echo $output;
        return $res;

        $url = "http://example.com";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"GET");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");        
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json_d); // arr
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //  return exec response as a string otherwise direct ouptput
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); //5 sec
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');

		curl_setopt($ch, CURLOPT_MAXREDIRS, 10);	
		curl_setopt($ch, CURLOPT_TIMEOUT , 0);	
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);	
		curl_setopt($ch, CURLOPT_HTTP_VERSION, 'CURL_HTTP_VERSION_1_1');	
        
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: '.strlen($json_d),
            'User-Agent: '.(request()->header('User-Agent')))
        );
        $res = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE); //$code == 200 || $code == 201 || $code == 500 || $code == 307)        
        if ($res === FALSE)
        {
            die('Curl failed: '.curl_error($ch));
        }
        curl_close($ch);
        $res = json_decode($res);
    }
    public function curl(Request $request)
    {
        $ip = '157.50.155.112';
        $ch = curl_init();
        $url = 'http://ipinfo.io/'.$ip.'/json';
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        #die('Curl failed: '.curl_errno($ch)); //0
        #die('Curl failed: '.curl_getinfo($ch, CURLINFO_HTTP_CODE)); //404 200
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($result === FALSE)
        {
            die('Curl failed: '.curl_error($ch));
        }
        #print_r($result); exit;
        return $result;
    }

    public function first(Request $request)
    {
        $response = [];
        if($request->isMethod('POST'))
        {                
            //print($request['city']); exit;

            $request->city = '';
            $request->state = '';
            unset($request['city'], $request['state']);

            // print_r($request->all());   
            // exit;   

            $rules = [
                //'picture'=>'required|file|mimetypes:image/jpg,image/jpeg,image/png|max:2048',
                'email'=>'required',
                'password'=>'required',
                'city'=>'required',
                'state'=>'required',
            ];
            $messages = [
                'email.required'=>'Email is required',
                'password.required'=>'Password is required',
                'city.required'=>'Password is required',
                'state.required'=>'Password is required',
            ];

            $validator = Validator::make($request->all(), $rules, $messages);

            //print_r($validator->errors()); exit;
            //print_r($validator->messages()); exit;

            if ($validator->fails()) {    
                return response()->json(['errors'=>$validator->messages()], 422);
                return response()->json(['errors'=>$validator->errors()], 422);
                return response()->json($validator->errors(), 422);
            }

            // if ($validator->fails()) {
            //     return redirect()->
            //             ->withErrors($validator)
            //             ->withInput();
            // }
            print_r($validator); exit;
            
            //@Datatable
            // $response['recordsTotal'] = $response['recordsFiltered'] = DB::table('users')->count();
            // $response['data'] = DB::table('users')->skip($request->start)->take($request->length)->get();
            // return response()->json($response, 200);
        }
        //$data = DB::table('users')->get()->all();
        $data = DB::table('users')->get();

        // print_r($data); 
        // exit;
        return view('test.first',compact('response','data'));
    }

    public function datatable(Request $request)
    {
        $response = [];
        if($request->isMethod('POST'))
        {
            //@Datatable
            $response['recordsTotal'] = $response['recordsFiltered'] = DB::table('users')->count();
            $response['data'] = DB::table('users')->skip($request->start)->take($request->length)->get();
            return response()->json($response, 200);
        }
        //$data = DB::table('users')->get()->all();
        $data = DB::table('users')->get();

        // print_r($data); 
        // exit;
        return view('test.first',compact('response','data'));
    }

    public function demo(Request $request)
    {
        $data = DB::table('users')->get()->all();
        $data = DB::table('users')->where('name','kavi')->first();        
        $data = DB::table('users')->where('name','kavi')->first();        
        $data = DB::table('users')->value('name');        
        $data = DB::table('users')->find('3');        
        $data = DB::table('users')->pluck('name');        
        $data = DB::table('users')->pluck('name','email');        
        $data = DB::table('users')->orderBy('email','DESC')->pluck('name','email');        
        $data = DB::table('users')->max('email');    
        $data = DB::table('users')->avg('email');   
        $data = DB::table('users')->where('name','kavi')->exists();  
        $data = DB::table('users')->where('name','kdddavi')->doesntExist();  
        $data = DB::table('users')->select('name','email')->get()->all();  
        $data = DB::table('users')->select('name','email')->addSelect('password','updated_at')->distinct()->get()->all();  
        // $data = DB::table('users')->select(DB::raw('count(*) as user_cnt, name, email'))->where('remember_token','')->groupBy('remember_token')->get();
        // $data = DB::table('users')->where('id','>', 5)->dd();  
        // $data = DB::table('users')->where('id','>', 5)->dump();  
        // $data = DB::table('users')->where('id','=', 5)->lockForUpdate()->get();  
        // $data = DB::table('users')->where('id','=', 5)->sharedLock()->get();  
        // $data = DB::table('users')->where('id','=', 5)->update([''=>'',''=>'']);  
        // $data = DB::table('users')->increment('ex_id');  
        // $data = DB::table('users')->increment('ex_id',3);  
        // $data = DB::table('users')->decrement('ex_id');  
        // $data = DB::table('users')->decrement('ex_id',3);  
        // $data = DB::table('users')->updateOrInsert(['name'=>'ram'],['email'=>'ram@gmail']);  
        // $data = DB::table('users')->insertGetId(['name'=>'ram','email'=>'ram@gmail']); 
        $data = DB::table('users')
                //->select(DB::raw('count(*) as user_cunt, name,email,country'))
                //->selectRaw('count(*) as user_cunt, name,email,country')
                //->selectRaw('count(flag) as flag_cunt, count(*) as user_cunt, name, email, country')
                //->selectRaw('count(flag) as flag_cunt, count(*) as user_cunt, flag, name, email, country')
                //->selectRaw('flag, SUM(flag) as flag_cunt, count(*) as user_cunt, name, email, country')
                //->selectRaw('flag, count(flag) as flag_cunt, count(*) as user_cunt, name, email, country')
                ->selectRaw('flag, SUM(flag) as sum_flag, count(*) as user_cunt, name, email, country')
                //->where('status',1)
                //->groupByRaw('country, flag')
                //->where('country','india')                
                ->groupBy('country')
                //->having('country','india')
                ->havingRaw('SUM(flag) > ?',[10])
                ->get()->all(); 

        // $data = DB::table('users')->selectRaw('flag, flag * ? * ? as flag_cunt',[5, 2])->get()->all(); 

        $data = DB::table('users as u')  
                ->join('tags as t','t.id','=','u.id')      
                // ->selectRaw('COUNT(u.id), MAX(u.id)')         
                // ->selectRaw('COUNT(u.id) as aaa, MAX(u.id) as bbb') 
                //->select('u.id','u.flag','u.name','u.email','t.name as tname')                
                //->select('u.id','u.flag','u.name','u.email', DB::raw('concat(t.id,\' - \',t.name,\' \','t.created_at') as tname'))                
                //->selectRaw('u.id,u.flag,u.name,u.email, concat(t.id,\' - \',t.name,\' \',t.created_at) as tname')                
                //->selectRaw('u.id,u.flag,u.name,u.email, concat_ws(\'-\',t.id,t.name, DATE(t.created_at)) as tname')                
                //->selectRaw('u.id,u.flag,u.name,u.email, concat_ws(\' - \',t.id,t.name, DATE_FORMAT(t.created_at,\'%d-%m-%y\')) as tname')                
                //->selectRaw('u.id,u.flag,u.name,u.email, GROUP_CONCAT(concat_ws(\' - \',t.id,t.name)) as tname')
                //->selectRaw('u.id,u.flag,u.name,u.email, GROUP_CONCAT(t.name ORDER BY t.name ASC SEPARATOR \' -- \') as tname')
                //->selectRaw('GROUP_CONCAT(t.name ORDER BY t.name ASC SEPARATOR \' -- \')')
                ->selectRaw('u.id,u.flag,u.name,u.email, GROUP_CONCAT(t.name ORDER BY t.name ASC SEPARATOR \' -- \') as tname')
                ->get()->all(); 

            $data = DB::table('users as u')  
                   //->join('tags as t','t.id','=','u.id')   
                   ->join('tags as t', function($join){
                        $join->on('t.id','=','u.id');
                             //->orOn()
                            //->where('t.name','Laravel');
                   })   
                   ->selectRaw('u.id,u.flag,u.name,t.name as tname')
                  ->get()->all(); 

            $tags = DB::table('tags')
                    ->select('id', 'name');
                    //->groupBy('id'); 

            $data = DB::table('users as u')  
                    ->joinSub($tags,'tag', 'tag.id', '=','u.id')
                    ->selectRaw('u.id,u.flag,u.name,tag.name as tname')
                    ->get()->all();
                    
                    
            $first = DB::table('tags')
                     ->select('name');                     
        
            $data = DB::table('users')
                    ->select('name')
                    ->union($first)
                    ->get()->all();

            $name= 'kavi';
            $data = DB::table('users as u')
                    // ->when($name, function($sub,$name) {
                    //     $sub->where('name',$name);
                    // })
                    // ->where(function($sub) use($name) {
                    //     $sub->where('name',$name);
                    // })
                    // ->where([
                    //     ['name','=','kavi'],
                    //     ['id','=',2]
                    // ])
                    //->whereBetween('id',[1,6])
                    //->whereNotBetween('id',[5,6])
                    //->whereIn('id',[5,6,8,9])
                    //->whereNotIn('id',[5,6,8,9])
                    //->whereNull('flag')
                    //->whereNotNull('flag')
                    //->whereDate('created_at','2020-10-12')
                    //->whereMonth('created_at','10')
                    //->whereDay('created_at','10')
                    //->whereYear('created_at','10')
                    //->whereTime('created_at','10:10:10')
                    //->whereColumn('created_at','updated_at')
                    // ->whereColumn([
                    //     ['created_at','>','updated_at'],
                    //     ['id','=','flag']
                    // ])
                    // ->whereExists(function($sub){
                    //     //$sub->select('id')
                    //     $sub->select(DB::raw(1))
                    //     ->from('tags')
                    //     ->where('tags.id','=','u.id');
                    // })
                    //->whereRaw('FIND_IN_SET(?,flag)',[3])
                    //->whereRaw('NOT FIND_IN_SET(?,flag)',[1])
                    ->selectRaw('id,flag,name')
                    ->get()->all();

        $this->select('name',
                DB::raw("(
                    SELECT  COUNT(id)
                    FROM    test 
                    WHERE   mid = :member AND created > :from AND created < :to
                    ) as numbers", [":member" => $member, ":from" => $from, ":to" => $to])
            )
            ->where('id', '=', '5')
            ->groupBy('type');
       

        
        echo "<pre>";
        print_r($data);
        exit;        
        
        return view('test.first',compact('response','data'));
    }

    public function demo2(Request $request)
    {
        # @@ JOINS  @@ #
        // SELECT SupplierName, exists(select * from Orders where OrderID = '102w8') as ord FROM Suppliers
        // SELECT * FROM posts CROSS JOIN posts; // CROSS JOIN
        // SELECT * FROM a, b; A tbl have 4 rcds and B table have 4 rcds return 4*4 16 rcds // CROSS JOIN
        // SELECT * FROM a where a.id = a.user_id; SELF JOIN Like WHERECOLUMN

        //$student = StudentDetails::create(['student_id'=> 3, 'class'=> 'AAAA', 'section'=> 'AAAA', 'address'=> 'Address1']);
        // $student = StudentDetails::create(['student_id'=> 2, 'class'=> 'BBB', 'section'=> 'BBB', 'address'=> 'Address1']);
        // $student = StudentDetails::create(['student_id'=> $std->id, 'class'=> 'CCC', 'section'=> 'CCC', 'address'=> 'Address1']);

        //$std = Student::with('details:student_id,section')->get()->all();
        //$std = StudentDetails::with('student:id,name,mobile,email')->get()->all();
        //$std = StudentDetails::where('student_id',1)->with('student:id,name,mobile,email')->get()->all();
        // $std =  Student::with('details')->find(1);
        // $std = $std->details()->where('section','AAAA')->get();

        $id = 1;
        $email = 'raj@gmail.com';
        $std =  Student::where(function($sub) use($id, $email) {
                    $sub->where('id',$id);
                    $sub->where('email',$email);
                })
                ->with('details')
                ->get()->all();

        $std =  Student::with('detailses')->find(1);
        $std =  Student::with('detailses')->find(1);
        $std =  Student::has('detailses')->with('detailses')->get();
        $std =  Student::has('detailses','>=',3)->with('detailses')->get()->all();
        $std =  Student::has('detailses','>=',3)
                ->whereHas('detailses',function($sub){
                   //$sub->where('class','=','AAAA');
                   //$sub->where('class','like','%AAAA%');
                   //$sub->where('section','like','%AAAA%');
                   $sub->where('section','=','AAAA');
                },'>',1)
                ->with('detailses')
                ->get()->all();

        // $std =  Student::doesntHave('detailses')->with('detailses')->get()->all(); //Dont Have deatails record
        // $std =  Student::whereDoesntHave('detailses',function($sub){   // Have a deatails record but doesnot have class 'FFFF' 
        //             $sub->where('class','=','FFFF');
        //         })
        //         ->with('detailses')->get()->all();

        $std =  Student::
                select('id','name','mobile')
                //->withCount(['detailses'])
                ->withCount(['detailses','detailses as deta_count' => function($qqq){
                    $qqq->where('class','like','%AAAA%');
                }])
                ->has('detailses')
                ->with('detailses')
                //->select('id','name','mobile','detailses_count')
                ->get()->all();
       
        $std =  Student::find(1);
        $std =  $std->loadCount('detailses');
        $std =  Mechanic::with('cars.owners')->get();
        $std =  User::with(['posts','addresses'])->get();
        $std =  User::has('addresses','>',2)->has('posts','>',1)->with(['posts','addresses'])->get();

        $std =  Student::query()
                ->with(['detailses'=> function($qqq){
                    //$qqq->where('class','like','%BBB%'); 
                    //$qqq->groupBy('section');
                    //$qqq->orderBy('id','DESC');
                    $qqq->selectRaw('student_id, id, class, section');
                    //$qqq->selectRaw('student_id, id, class, section, COUNT(student_id) as student_count');
                    //$qqq->selectRaw('student_id, id, class, section, COUNT(*) as sec_count, MAX(student_id) as max_count');
                    //$qqq->groupBy('student_id');
                    //$qqq->groupBy('section');
                }])
                //with('detailses')
                ->selectRaw('id, name, mobile, email')
                ->withCount('detailses')
                ->get();

        $std =  Student::find(2);
        $std =  $std->loadCount('detailses as ccc');

        $std =  Student::pluck('name','id');

        //$std =  Student::find([1,2,3]);
        //$std =  Student::find(1)->details;
        $std =  Mechanic::find(1)->cars;
        
        $std =  comment::whereHasMorph('subject','*')->get();
        $std =  comment::whereHasMorph('subject',['App\Post','App\Video'])->get();
        $std =  comment::whereHasMorph('subject',['App\Post'])->get();
        $std =  comment::whereHasMorph('subject',['App\Post','App\Video'],function($qqq){
                    $qqq->where('body','First Video Comment..');
                })->get();

        $std =  comment::whereDoesntHaveMorph('subject',['App\Post','App\Video'],function($qqq){
                    $qqq->where('body','First Video Comment..');
                })->get();

        $std =  comment::whereHasMorph('subject',['App\Post','App\Video'],function($qqq , $type){
                    $qqq->where('commentable_id',2);
                    if($type === 'App\Post'){
                        $qqq->Where('body','First Post Comment..');
                        //$qqq->orWhere('body','First Post Comment..');
                    }
                })->get();

        $std =  comment::with(['subject'])->get();

        $std =  comment::with(['subject'=> function(MorphTo $morphTo){
                    $morphTo->morphWithCount([
                        Post::class=>['comments'],
                        Video::class=>['comments']
                    ]);                   
                }])
                ->get();

        $std =  comment::with(['subject'=> function(MorphTo $morphTo){
                    //$morphTo->morphWithCount([
                    $morphTo->morphWith([
                        Post::class=>['comments'],
                        Video::class=>['comments']
                    ]);                   
                }])
                ->get();

        $std = comment::with('subject')
                ->get()
                ->loadMorph('subject', [
                    Post::class=>['comments'],
                    Video::class=>['comments']
                ]);

        $std = comment::with('subject')
                ->get()
                ->loadMorphCount('subject', [
                    Post::class=>['comments'],
                    Video::class=>['comments']
                ]);

        $std = Post::with('comment','comments','motags')->find(1);
        $std = Post::find(1);

        $std1 = new comment(['user_id'=>2, 'body'=>'Second Post Comment..']);
        //$std = $std->comment()->save($std1);
        //$std = $std->comments()->save($std1);
        //$std = $std->comments()->create(['user_id'=>2, 'body'=>'Second Post Comment..']);
        //$std = $std->comments()->createMany(['user_id'=>2, 'body'=>'Six Post Comment..'],['user_id'=>2, 'body'=>'Five Post Comment..']);
 
        // $std->comment[0]->body = 'First Post Commentssss..';  
        // $std->push(); // Not Work

        $std = Video::find(1);
        $comment = comment::find(7);

        // $std->comment()->associate($comment); // Only Belongs to relationship
        // $std = $std->save();
        //$std = $std->comment;
        $std = $std->comment->body;

        
        // $user = User::find(2);
        // $addr = Address::find(7);
        // $addr = new Address(['account_id'=>3,'country'=>'Bermuda']);
        // $addr->account()->associate($user);
        // //$addr->account()->dissociate($user);
        // $addr->save();

        // $user = User::find(1);
        // $addr = Address::find(7);
        // $user->addresses()->associate($addr); // Has One Not Associate
        // $addr->save();
        
        // echo "<pre>";
        // print_r([$user,$addr]);
        // exit;

        //print_r($std[2]->email); exit;

        $users = DB::table('users')
                ->whereExists(function ($query) {
                    $query->select('title')
                            ->from('posts')
                            //->where('posts.title', '=', 'Post 3')
                            ->whereRaw('posts.user_id = users.id');                            
                })
                ->get();

      
        
        return $users;
    }
}
