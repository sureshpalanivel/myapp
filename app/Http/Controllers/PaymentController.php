<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Builder;

use App\User;
use App\Address;
use App\Post;
use App\Tag;
use App\Mechanic;
use App\Car;
use App\Owner;
use App\Video;
use App\Comment;
use App\Demo;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Request\UploadRequest;
use Illuminate\Support\Str;

use Razorpay\Api\Api;

class PaymentController extends Controller
{  
    private $razorpayId = 'rzp_test_BUshunaWjKDWKS'; //'rzp_test_Y2zzzKcOyIGQKB';
    private $razorpayKey = 'VHEIDqgv19FeynIjmIgxTmnn'; //'6VmT4OEdg2Z1fIvGUQE6TDBp'; 

    public function paymentInitiate(Request $request)
    {
        return view('pgw.payment-gateway-initiate');
    }

    public function paymentInitiateReq(Request $request)
    {
        $amount = 200;
        $name = 'apple';
        $description = 'Natural fruit';
        $email = 'apple$gmail.com';
        $contact = '9876543210';
        $address = 'Apple Shop';

        $api = new Api($this->razorpayId, $this->razorpayKey);
        
        $receipt_id = Str::random(20);       

        $order = $api->order->create(array(
            'receipt' => $receipt_id,
            'amount' => $amount *100, // COnverted into paisa
            'payment_capture' => 1,
            'currency' => 'INR'
            )
        );

        //print_r($order['id']); exit;

        $response = [
            'razorpayId' => $this->razorpayId,
            'amount' => $amount *100, // COnverted into paisa
            'currency' => 'INR',
            'name' => $name,
            'description' => $description,
            'order_id' => $order['id'],
            'email' => $email,
            'contact' => $contact,
            'address' => $address,
        ];

        #print_r([$order, $response]); exit;

        #return view('pgw.payment-gateway',compact('response'));
        return view('pgw.test',compact('response'));
    }   
}
