<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    //
    protected $guarded = [];

    public function comments()  //return collections
    {
        return $this->morphMany(Comment::class, 'commentable');      
    }

    public function comment()  //return single
    {
        return $this->morphOne(Comment::class, 'commentable');      
    }

    public function motags()  //return single
    {
        return $this->morphToMany(Tag::class, 'taggable');    
    }
}
