<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    //
    protected $fillable = [
        'model', 'mechanic_id',
    ];

    public function mechanics()
    {
        return $this->belongsTo(Mechanic::class, 'mechanic_id', 'id');
    }

    public function owners()
    {
        return $this->hasOne(Owner::class, 'car_id', 'id');
    }
}
